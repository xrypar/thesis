# Thesis

This is the repo with configuration files and copy of WalletWasabi commit the setup runs on.

## What is needed to run the setup

1. The dotnet 7 SDK, as the WalletWasabi runs in the dotnet environment.
1. Installed and synced bitcoin core node.


## How to run the test setup

### Using TestNet (Deprecated)
1. Clone this repository.
1. Copy the BitcoinCore/TestNet/bitcoin.conf to the location your bitcoin core configuration file is expected (or copy its contents to your config file)
1. Copy the contents of *AplicationFilesChanged/TestNetConfs* folder to the location where your application data are usually located (for the Windows OS, it would be %APPDATA% folder/WalletWasabi)
    1. Usual Windows path: C:\Users\username\AppData\Roaming\WalletWasabi
    1. Usual Linux path: /home/username/.walletwasabi/
1. Set needed options in Global Constants file

Note: The setup may not work correctly as the developement was moved to the RegTest and later stoped at all



### Using RegTest
1. Clone this repository.
1. Copy the BitcoinCore/RegTest/bitcoin.conf to the location your bitcoin core configuration file is expected (or copy its contents to your config file)
1. Copy the contents of *AplicationFilesChanged/RegTestConfs* folder to the location where your application data are usually located (for the Windows OS, it would be %APPDATA% folder/WalletWasabi)
    1. Usual Windows path: C:\Users\username\AppData\Roaming\WalletWasabi
    1. Usual Linux path: /home/username/.walletwasabi/
1. Set needed options in Global Constants file

#### First time using RegTest
If you are using regtest for the first time, you need to run few commands or script to get it started.

##### Script
1. Start bitcoin core (open bitcoin-qt) manually
1. Fill addresses to your Wallet Wasabi data folder in file global_constants.py 
1. Run thesis\Scripts\init_regtest.py script and wait until it finishes


##### Manual
 On Windows, use the commands as `.\bitcoin-cli.exe ...`. 

1. Start bitcoin core (open bitcoin-qt) manually
1. Run command `bitcoin-cli createwallet "wallet"` to create initial wallet for your bitcoin core. If you changed rpc username and/or password, change it also here. You can also use your own name of wallet.
1. Run command `bitcoin-cli -generate 101` to mine first 101 blocks to your wallet.

Now, your bitcoin core regtest setup is ready. Every time you want to mine new block, you can use command `bitcoin-cli -generate 1`.
Using this alternative, you still need to create Distributor wallet and fill it with funds manually.


### Running the setup
1. Start bitcoin core and wait for it to synchronize
1. Make sure you have setup all values in the file Scripts/Helpers/global_constatns.py
1. Prepare scenario based on the description in file Scripts/README.md and save it to file *scenario.json* saved in the Sctipts folder
1. Run the Scripts/scenarios.py script
1. The setup should be running now

### Important
Be careful with the passwords for RPC server of your bitcoin core, the values are setup to "default", example values. If you wish to use different pair of username and password (as sugested), change lines `rpcuser` and `rpcpassword` in *thesis/BitcoinCore/bitcoin.conf* file and also line `BitcoinRpcConnectionString` to format "new_username:new_password" in *thesis/AplicationFilesChanged/Backend/Config.json*

### Notes
- Backend won't run without the bitcoin core running.
- Client app won't run without the backend running (It should not crash, but will fail to open any wallet).
- Passwords of all wallets up to this date (12.07.2023) are set to *pswd*


